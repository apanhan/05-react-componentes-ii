import React from 'react'

const User = props => {
    return (
        <div className={`user-${props.name}`}>
            <h1>Olá {props.name}</h1>
            {props.children}
        </div>
    )
}

export default User